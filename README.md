# test tuning ML lib models
## Abs:
Tuning models is an explanatory phase, we can go random or we can go through a grid search.

## where to start?
Some people like to read all the docs before starting to code. I rather like to start deploying examples as a bootstrap.

## Tensorflow
find a model/dataset that is easy to train and deploy with hyper parameters.

## hypertuning readme


<h4 id="h-samplers-algorithms-available-in-optuna"><strong>Samplers Algorithms available in Optuna</strong></h4>



<ul><li><strong>&nbsp;</strong>Model-based<ul><li><a href="https://papers.nips.cc/paper/2011/file/86e8f7ab32cfd12577bc2619bc635690-Paper.pdf" data-wpel-link="external" target="_blank" rel="follow">TPE : Bayesian optimization based on kernel fitting.</a></li><li><a href="https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&amp;arnumber=7352306" data-wpel-link="external" target="_blank" rel="follow">GP : Bayesian optimization based on Gaussian processes.</a></li><li><a href="http://www.cmap.polytechnique.fr/~nikolaus.hansen/cmaartic.pdf" data-wpel-link="external" target="_blank" rel="follow">CMA-ES : meta-heuristic algorithm for continuous space.</a></li></ul></li><li>Other methods<ul><li>Random Search</li><li>Grid Search</li><li>User-defined algorithm</li></ul></li></ul>



<h4 id="h-pruning-algorithms-available-in-optuna"><strong>Pruning Algorithms available in Optuna</strong></h4>



<ul><li><a href="https://arxiv.org/pdf/1810.05934.pdf" data-wpel-link="external" target="_blank" rel="follow">Asynchronous Successive Halving algorithm</a></li><li><a href="https://arxiv.org/abs/1603.06560" data-wpel-link="external" target="_blank" rel="follow">Hyperband algorithm&nbsp;</a></li><li>Median pruning algorithm&nbsp;</li><li>Threshold pruning algorithm&nbsp;</li></ul>



---
## Designed with M1 devices in mind
---
## SETUP:

- miniforge / conda / miniconda
- pip for compatibility with docker (more on that topic later)
- python 3.10 (released 2 days ago with apple support, what a coincidence!)
- apple blob

## RUN:
```s
conda env create -f environment.yml
conda install -c apple tensorflow-deps
jupyter notebook --port=8888 
```
or run the python script 5 seconds to see if it segfaults

## Workflows
### Conda
Conda will serve as a wrapper of pip, this way our docker would use pip as usual but our environment would still be contained wihtin a conda env.  
*example*
```
cd 5.frontend
conda env create -f environment.yml
conda activate ai_nlp_travel
```
### missing a dep? 

run `conda install requests -c conda-forge`
conda is a pre-containerization step before packaging the app into a docker image (which don't require conda or moni-conda because... well it's a container!)

## Gitops features {Free Cookie}
- clean notebook : 
    - append to .git/config `[filter "remove-notebook-output"]
    clean = "jupyter nbconvert --clear-output --to=notebook --stdin --stdout --log-level=ERROR"`
    - create a gitattributes files with `*.ipynb clean = 1`

## TROUBLESHOOTING:
- plop



