# hyperparameter tuning job
Remarks, no UI as is, not clear when it should stop. --s Probably when the learning rate is too low. --
stop after 100 trials

good thing, gpu is detected.

logs:
```s
I 2022-05-26 15:57:32,004] Trial 64 finished with value: 0.9510000348091125 and parameters: {'filters': 32, 'kernel_size': 5, 'strides': 1, 'activation': 'relu', 'learning_rate': 0.007474472695413963}. Best is trial 59 with value: 0.956000030040741.
2022-05-26 15:57:32.344918: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
2022-05-26 15:57:32.779773: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
[I 2022-05-26 15:57:35,769] Trial 65 finished with value: 0.9410000443458557 and parameters: {'filters': 32, 'kernel_size': 5, 'strides': 1, 'activation': 'relu', 'learning_rate': 0.0016996009039010902}. Best is trial 59 with value: 0.956000030040741.
2022-05-26 15:57:36.089785: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
2022-05-26 15:57:36.559090: I tensorflow/core/grappler/optimizers/custom_graph_optimizer_registry.cc:113] Plugin optimizer for device_type GPU is enabled.
```